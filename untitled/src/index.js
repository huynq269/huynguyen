// ...
import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import store from './store/store';
import App from './component/app-menu/app';
// ...

console.log(store.getState);
console.log("App run");
render(
    <Provider store = {store}>
        <App/>
    </Provider>,
	document.getElementById('root'));

