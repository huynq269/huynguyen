export function changeLink(indexLink){
    return {type: "CHANGE_LINK", indexLink}
}
export function showNote(indexNote){
    return {type: "CHANGE_NOTE", indexNote}
}
