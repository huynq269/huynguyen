import React from 'react';
import {connect} from 'react-redux';
import '../css/stylehello.css';
import {changeLink} from '../../../actions/action';

class Hello extends React.Component{
    constructor(props){
        super(props);
        this.showLink = this.showLink.bind(this);
    }
    changeLink(i){
        var {dispatch} = this.props;
        dispatch(changeLink(i));
        
    }
    showLink(){
        switch(this.props.linkReducer){
            case 1:
                return(<a href = "https://www.youtube.com/watch?v=tkAwq2_9Ndg"> Video Happy New Year</a>);
            case 2:
                 return(<a href = "https://www.youtube.com/watch?v=3Wv-JIpAjks"> Video Merry Christmas</a>);
            case 3:
                 return(<a href = "https://www.youtube.com/watch?v=JrFZhHizBFM"> Video Valentine</a>);
            default:
                return null;
        }
    }
    showHome(){
        window.location.href = window.location.href.substring(0,window.location.href.length-5);
    }

    render(){
        return(
            <div className="wrapper">
                <div className="hello"> Hello </div>
                <button onClick = {this.showHome}> Pre </button>
                <div className="list-item">
                    <ul>
                        <li><button onClick = {()=>this.changeLink(1)}>Happy new Year</button></li>
                        <li><button onClick = {()=>this.changeLink(2)}>Merry Christmas</button></li>
                        <li><button onClick = {()=>this.changeLink(3)}>Valentine</button></li>
                    </ul>
                </div>
                {this.showLink()}
            </div>

        );
    }
}
export default connect(function(state){
    return {linkReducer: state.linkReducer}
    })(Hello);