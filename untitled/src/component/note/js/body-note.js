import React from 'react';

class Body extends React.Component{
	render(){
		return(
			<div>
				<div className = "body-top"> Accounting Dashboard </div>
				<div className = "body-list">
					<ul>
						<li className = "item-body-list"> Configuration Steps </li>
						<li className = "item-body-list"> COMPANY DATA </li>
						<li className = "item-body-list"> BANK ACCOUNTS </li>
						<li className = "item-body-list"> FISCAL YEAR</li>
						<li className = "item-body-list"> CHART OF ACCOUNTS</li>
						<li className = "item-body-list"> INITIAL BALANCES</li>
					</ul>
				</div>
				<div className = "body-content"> </div>
			</div>

		);
	}
}
export default Body;