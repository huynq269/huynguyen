import React from 'react';
import Body from './body-note.js';
import BodySales from './body-sales.js';
import {connect} from 'react-redux';
class TagNode extends React.Component{
	constructor(props){
		super(props);
	}
	showHome(){
		console.log("click");
		window.location.href = window.location.href.substring(0,window.location.href.length-5);
	}
	showInfo(i){
		var {dispatch} = this.props;
		dispatch({
			type: "CHANGE_NOTE",
			indexNote: i
		});
	}
	renderInfo(){
		if(this.props.showNoteReducer === 1){
			return(
				<Body/>
			);
		}
		if(this.props.showNoteReducer === 2){
			return(
				<BodySales/>
			);
		}
		return null;
	}
	render(){
		return(
			<div> 
				<div className = "header">
					<button onClick = {this.showHome.bind(this)}><img src = "http://homecareconnection.hdsmith.com/imgs/arrow_LeftLight.png"/></button>
					<div className = "name"> Acouting </div>
					<ul> 
						<li className = "item-list"><button onClick = {()=>this.showInfo(1)}> Dashboadrd </button></li>
						<li className = "item-list"><button onClick = {()=>this.showInfo(2)}>Sales</button> </li>
						<li className = "item-list"><button onClick = {()=>this.showInfo(3)}> Purchases</button> </li>
						<li className = "item-list"><button onClick = {()=>this.showInfo(4)}>Adviser </button></li>	
						<li className = "item-list"><button onClick = {()=>this.showInfo(5)}> Reporting </button></li>
						<li className = "item-list"><button onClick = {()=>this.showInfo(6)}> Configuration</button> </li>
					</ul>
				</div>
				{this.renderInfo()}
			</div>
		);
	}
}

export default connect(function(state){
	return {showNoteReducer: state.showNoteReducer}
})(TagNode);