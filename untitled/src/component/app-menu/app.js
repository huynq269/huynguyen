import Note from '../../component/note/js/note.js';
import listItems from '../../model/data.js';
import ImgButton from '../../component/imgbutton/js/ImgButton';
import Hello from '../../component/helloworld/js/hello';
import React from 'react';
class App extends React.Component{
    constructor(props){
        super(props);
    }
    findGetParameter(parameterName) {
        var result = null,
            tmp = [];
        (window.location).search
            .substr(1)
            .split("&")
            .forEach(function (item) {
                tmp = item.split("=");
                if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
            });
        return result;
    }
    render(){
        var itemID = this.findGetParameter("id");
        if(itemID == null){
            return(
                <div>
                    {listItems.map(function(item, i){
                        return <ImgButton id={i} obj = {item} key={i} content = {item.content} link = {item.link}/>;
                    })}
                </div>
            );
        }
        if(itemID == 0) return <Note/>;
        if(itemID == 1) return <Hello/>;
    }
}
export default App;