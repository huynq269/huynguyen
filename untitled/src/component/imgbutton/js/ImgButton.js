import React from 'react';
import '../css/stylemodel.css';

class ImgButton extends React.Component {
    showMe() {
        window.location.href = window.location.href + '?id=' + this.props.id;
    }

    render() {
        console.log(this.props);
        return (
            <div className="wrapper">
                <div className="item">
                    <button onClick={this.showMe.bind(this)}>
                        <img src={this.props.link}/>
                        <div className="content">{this.props.content}</div>
                    </button>
                </div>
            </div>
        );
    }
}

export default ImgButton;