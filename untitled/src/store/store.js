
import {createStore} from 'redux';
import reducers from '../reducers/reducers';

var store = createStore(reducers);

export default store;