var linkReducer = (state = 0,action) => {
    switch(action.type){
        case "CHANGE_LINK":
            return action.indexLink;
        default:
            return state; 
    }
}

export default linkReducer;