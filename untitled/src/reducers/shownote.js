var showNoteReducer = (state = 0, action) => {
    switch(action.type){
        case "CHANGE_NOTE":
            return action.indexNote;
        default:
            return state;
    }
}
export default showNoteReducer;