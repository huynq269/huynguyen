import { combineReducers } from 'redux'
import linkReducer from './link.js';
import showNoteReducer from './shownote.js';

var reducers = combineReducers({linkReducer, showNoteReducer});

export default reducers;